# SamplePOC_BLE #

Observe the scanning results in the foreground and background with details in the console log..

### What is this repository for? ###

To test the GATT service UUID scanning in the background

### How do I get set up? ###

- Run on device >= 13.0
- Before running the project, open Constant.swift file and check the property allUsersToScanFor if the profile ID is present on which you are advertising.
- Run the project on the real device and give the permision for location and bluetooth.


### Who do I talk to? ###

Email: salman@cuttingchaitech.com
