//
//  AppDelegate.swift
//  SampleAppBLE_POC
//
//  Created by Salman Qureshi on 7/15/20.
//  Copyright © 2020 Salman Qureshi. All rights reserved.
//

import UIKit
import CoreBluetooth
import CoreLocation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate {

    private var peripheralManager = CBPeripheralManager()
    private var centralManager = CBCentralManager()
    private var locationManager = CLLocationManager()
    
    private var serviceUUID: [CBUUID] = []
    
    private var txService = CBUUID()
    
    private var discoveredProfileIds:[String] = []
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        requestLocation()
        hds()
        return true
        
        
    }
    
    private func requestLocation() {
        locationManager.requestAlwaysAuthorization()
        locationManager.delegate = self
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.pausesLocationUpdatesAutomatically = false
        locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
        
        locationManager.startUpdatingLocation()
        
        if #available(iOS 13, *) {
            
            let beaconContraint = CLBeaconIdentityConstraint(uuid: UUID(uuidString: "2F234454-CF6D-4A0F-ADF2-F4911BA9FFA6")!)
            
            locationManager.startRangingBeacons(satisfying: CLBeaconRegion(proximityUUID: beaconContraint.uuid, major: 1, minor: 2, identifier: "dummy-beacon-region").beaconIdentityConstraint)
        } else {
            locationManager.startRangingBeacons(in: CLBeaconRegion(proximityUUID: UUID(uuidString: "2F234454-CF6D-4A0F-ADF2-F4911BA9FFA6")!, identifier: "dummy-beacon-region"))
        }
        

        //  requestLocation()
    }


    func hds() {
        txService = CBUUID(string: Constant.advertisingID)
        
        //Get all UUID to scan for
        Constant.allUsersToScanFor.forEach { (userId) in
            let eachStoreUUID = CBUUID(string: userId)
            serviceUUID.append(eachStoreUUID)
        }
        
        //serviceUUID.append(contentsOf: OverflowAreaUtils.allOverflowServiceUuids())
        
        startScanning()
        peripheralManager = CBPeripheralManager(delegate: self, queue: nil)
    }
    
     private func startScanning() {
            
            centralManager = CBCentralManager(delegate: self, queue: DispatchQueue.main)
            
        }
        
        private func startAdvertising() {
            
            if (peripheralManager.isAdvertising) {
                peripheralManager.stopAdvertising()
            }
            
            let service = CBMutableService(type: txService, primary: true)
            peripheralManager.add(service)
            
            let advertisingData: [String : Any] = [
                CBAdvertisementDataServiceUUIDsKey: [service.uuid]
            ]
            
            peripheralManager.startAdvertising(advertisingData)
        }
        
        public func stopAdvertising() {
            peripheralManager.stopAdvertising()
        }
    }

extension AppDelegate: CBCentralManagerDelegate {
        
        func centralManagerDidUpdateState(_ central: CBCentralManager) {
            //print("\(#line) \(#function)")
            
            if central.state == .poweredOn {
                /** Scan for peripherals
                 */
               //  serviceUUID.append(CBUUID(string: "00000000-0000-0000-0000-000000000039")
                
            centralManager.scanForPeripherals(withServices: serviceUUID, options: [CBCentralManagerScanOptionAllowDuplicatesKey: true])
                print("Scanning started")
            }
            else if central.state == .poweredOff {
                print("Scanning poweredOff")
            }
            else if central.state == .unsupported {
                print("\(#line) \(#function) unsupported")
            }
            else if central.state == .unauthorized {
                
                print("\(#line) \(#function) unauthroized")
            }
            else if central.state == .resetting {
                print("\(#line) \(#function) resetting")
            }
            else {
                print("\(#line) \(#function) anything else")
            }
        }
    
        func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        
            
           
            
            
            
            
            
            if OverflowAreaUtils.extractOverflowAreaToString(advertisementData: advertisementData) != "" {
               print("Service UUID when advertising in background: \(OverflowAreaUtils.extractOverflowAreaToString(advertisementData: advertisementData)) --> rssi: \(RSSI)")
            } else {
                 print("Service UUID when advertising in foreground: \(advertisementData["kCBAdvDataServiceUUIDs"] as? [CBUUID]) --> rssi: \(RSSI)")
            }
            
            //print("\(advertisementData) --> rssi: \(RSSI)")
//
//            var profileID = ""
//
//            if let _profileId = advertisementData[CBAdvertisementDataOverflowServiceUUIDsKey] as? [CBUUID] {
//
//                print("----------------------Central Discover----------------------")
//
//                print("ADVERTISEMENT DATA -> \(advertisementData), RSSI -> \(RSSI)")
//
//                //kCBAdvDataServiceUUIDs
//                print("Profile ID: \(_profileId.enumerated())")
//
//                let _id = _profileId[0]
//                profileID = _id.representativeString()
//                profileID = profileID.lowercased()
//
//                print("----------------------End Central Discover----------------------\n\n\n")
//
//            }
            
    //        if let profileUUID = advertisementData["kCBAdvDataServiceUUIDs"] as? [CBUUID] {
    //            //kCBAdvDataServiceUUIDs
    //            print("Profile ID: \(profileUUID.enumerated())")
    //
    //            let _id = profileUUID[0]
    //            profileID = _id.representativeString()
    //            profileID = profileID.lowercased()
    //        }
            
            
//            if profileID != "" && discoveredProfileIds.contains(profileID) == false {
//
//                self.discoveredProfileIds.append(profileID)
//                print("Discovered Profiles: \(discoveredProfileIds)")
//            }
            
            
        }
        

    }

    extension AppDelegate: CBPeripheralManagerDelegate {
        
        func peripheralManagerDidStartAdvertising(_ peripheral: CBPeripheralManager, error: Error?) {
            
            if (error != nil) {
                print("Error in the advertising: \(String(describing: error?.localizedDescription))")
            }
            
            print("Is peripheral advertising? : \(peripheral.isAdvertising)")
            print(peripheral)
        }
        
        public func peripheralManagerDidUpdateState(_ peripheral: CBPeripheralManager) {
            
            if (peripheral.state == .poweredOn) {
                startAdvertising()
                print("\(#line) \(#function) poweredOn")
            }
            else if peripheral.state == .poweredOff {
                peripheralManager.stopAdvertising()
                print("\(#line) \(#function) poweredOff")
            }
            else if peripheral.state == .unsupported {
                peripheralManager.stopAdvertising()
                print("\(#line) \(#function) unsupported")
            }
            else if peripheral.state == .unauthorized {
                peripheralManager.stopAdvertising()
                print("\(#line) \(#function) unauthorized")
            }
            else if peripheral.state == .resetting {
               peripheralManager.stopAdvertising()
               print("\(#line) \(#function) resetting")
            }
        }
    }


    extension CBUUID {
        
        func representativeString() -> String {
            let data = self.data
            
            let bytesToConvert = data.count
            let uuidBytes = data //  UnsafePointer<CUnsignedChar>(data.bytes)
            var outputString = String()
            
            for currentByteIndex in 0..<bytesToConvert {
                switch currentByteIndex {
                case 3,5,7,9:
                    outputString += String(format: "%02x-",uuidBytes[currentByteIndex])
                default:
                    outputString += String(format: "%02x",uuidBytes[currentByteIndex])
                }
            }
            
            return outputString
        }
    }

